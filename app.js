import {AppController} from './app.controller.js';

const app = new AppController;
app.init();

document.querySelector('.navbar-start .navbar-item').addEventListener(
  'click', app.routerLink, false
)