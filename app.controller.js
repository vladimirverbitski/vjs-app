'use strict';
import {AppService} from './app.service.js';

export class AppController {
  constructor() {
    this.appService = new AppService;
  }

  createIntroBlock() {
    const container = document.createElement('div');
    container.className = 'container';

    const not = document.createElement('div');
    not.className = 'notification';
    not.innerHTML = 'Welcome to my App!';

    container.append(not);
    document.body.prepend(container);
  }

  init() {
    this.createIntroBlock();
  }

  routerLink(e) {
    e.preventDefault();
    const href = e.srcElement.href;
    const urlPos = e.srcElement.href.lastIndexOf('/');
    const url = href.substr(urlPos + 1, href.length - 1);
  }

}